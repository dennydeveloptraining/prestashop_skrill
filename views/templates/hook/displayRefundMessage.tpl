{**
* 2017 Skrill
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
*
*  @author Skrill <contact@skrill.com>
*  @copyright  2017 Demoshop
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*}


    {if !empty($warningMessage)}
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {if $warningMessage == "refund"}
                Your attempt to refund the payment is pending.
            {/if}
        </div>
    {/if}
    {if !empty($successMessage)}
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {if $successMessage == "refund"}
               Your attempt to refund the payment success.
            {/if}
        </div>
    {/if}
    {if !empty($errorMessage)}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {if $errorMessage == "refund"}
               Unfortunately, your attempt to refund the payment failed.
            {/if}
        </div>
    {/if}
