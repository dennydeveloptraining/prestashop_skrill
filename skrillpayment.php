<?php
/**
*  @author    Sutrisno Denny
*  @copyright DS 2017
*  @license   DS ESPHERE @2017
*  @version   1.0.1
*
* Languages: EN
* PS version: 1.7.0.6
**/

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit();
}

class SkrillPayment extends PaymentModule
{
    public $processedStatus = 2;
    public $pendingStatus = 0;
    public $failedStatus = 2;

    public function __construct()
    {

        $this->name = 'skrillpayment';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.1';
        $this->author = 'Payreto';
        $this->need_instance = 1;
        $this->ps_versions_compliancy = array('min' => '1.7.0.0', 'max' => _PS_VERSION_);

        $this->displayName = $this->l('Skrill Payment');
        $this->description = $this->l('Payment Gateway that trusted in more than 42 Countries');

        $this->controllers = array('validation','paymentStatus');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $config = Configuration::getMultiple(
            array('SKRILL_PAY_ID', 'SKRILL_PAY_EMAIL',
            'SKRILL_RECIPIENT', 'API_PASSWORD', 'SECRET_PASSWORD', 'ENABLE_SKRILL', 'ENABLE_CC')
        );
        if (!empty($config['SKRILL_PAY_ID'])) {
            $this->id = $config['SKRILL_PAY_ID'];
        }
        if (!empty($config['SKRILL_PAY_EMAIL'])) {
            $this->email = $config['SKRILL_PAY_EMAIL'];
        }
        if (!empty($config['SKRILL_RECIPIENT'])) {
            $this->recipient = $config['SKRILL_RECIPIENT'];
        }
        if (!empty($config['API_PASSWORD'])) {
            $this->api_password = $config['API_PASSWORD'];
        }
        if (!empty($config['SECRET_PASSWORD'])) {
            $this->secret_password = $config['SECRET_PASSWORD'];
        }
        if (!empty($config['ENABLE_SKRILL'])) {
            $this->enable_skrill = $config['ENABLE_SKRILL'];
        }
        if (!empty($config['ENABLE_CC'])) {
            $this->enable_cc = $config['ENABLE_CC'];
        }

        $this->bootstrap = true;
        parent::__construct();

        if (!isset($this->id)
            || !isset($this->email)
            || !isset($this->recipient)
            || !isset($this->api_password)
            || !isset($this->secret_password)
            || !isset($this->enable_skrill)
            || !isset($this->enable_cc)
        ) {
            $warningCode = 'Skrill details must be configured before using this module.';
            $this->warning = $this->trans($warningCode, array(), 'Modules.SkrillPayment.Admin');
        }
    }

    public function install()
    {

        if (!parent::install()
            || !$this->registerHook('UpdateOrderStatus')
            || !$this->registerHook('paymentOptions')
            || !$this->registerHook('displayInvoice')
        ) {
            return false;
        }

        $this->addSkrillOrderStatus();
        $query = "CREATE TABLE sid(id INT NOT NULL AUTO_INCREMENT, order_id VARCHAR(50) NOT NULL,
        transaction_id VARCHAR(50) NOT NULL, status INT NOT NULL, PRIMARY KEY (id))";
        Db::getInstance()->execute($query);

        return true;
    }

    public function uninstall()
    {

        if (!Configuration::deleteByName('SKRILL_PAY_ID')
            || !Configuration::deleteByName('SKRILL_PAY_EMAIL')
            || !Configuration::deleteByName('SKRILL_RECIPIENT')
            || !Configuration::deleteByName('API_PASSWORD')
            || !Configuration::deleteByName('SECRET_PASSWORD')
            || !Configuration::deleteByName('ENABLE_SKRILL')
            || !Configuration::deleteByName('ENABLE_CC')
            || !parent::uninstall()
        ) {
              return false;
        }

        return true;
    }

    protected function postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            if (!Tools::getValue('SKRILL_PAY_ID')) {
                $errorMessage = "Skrill Merchant ID are required";
                $this->_postErrors[] = $this->trans($errorMessage, array(), 'Modules.SkrillPayment.Admin');
            } elseif (!Tools::getValue('SKRILL_PAY_EMAIL')) {
                $errorMessage = "Skrill Email is required";
                $this->_postErrors[] = $this->trans($errorMessage, array(), 'Modules.SkrillPayment.Admin');
            } elseif (!Tools::getValue('SKRILL_RECIPIENT')) {
                $errorMessage = "Skrill Recipient is required";
                $this->_postErrors[] = $this->trans($errorMessage, array(), 'Modules.SkrillPayment.Admin');
            } elseif (!Tools::getValue('API_PASSWORD')) {
                $errorMessage = "Skrill Api Password is required";
                $this->_postErrors[] = $this->trans($errorMessage, array(), 'Modules.SkrillPayment.Admin');
            } elseif (!Tools::getValue('SECRET_PASSWORD')) {
                $errorMessage = "Skrill Secret Password is required";
                $this->_postErrors[] = $this->trans($errorMessage, array(), 'Modules.SkrillPayment.Admin');
            }
        }
    }

    public function addSkrillOrderStatus()
    {
        $stateConfig = array();
        try {
            $stateConfig['color'] = 'blue';
            $this->addOrderStatus(
                'SKRILL_PAYMENT_STATUS_PENDING',
                'Pending',
                $stateConfig
            );
            return true;
        } catch (Exception $exception) {
            return false;
        }
    }

    public function addOrderStatus($configKey, $statusName, $stateConfig)
    {
        if (!Configuration::get($configKey)) {
            $orderState = new OrderState();
            $orderState->name = array();
            $orderState->module_name = $this->name;
            $orderState->send_email = true;
            $orderState->color = $stateConfig['color'];
            $orderState->hidden = false;
            $orderState->delivery = false;
            $orderState->logable = true;
            $orderState->invoice = false;
            $orderState->paid = false;
            foreach (Language::getLanguages() as $language) {
                $orderState->template[$language['id_lang']] = 'payment';
                $orderState->name[$language['id_lang']] = $statusName;
            }

            if ($orderState->add()) {
                $skrillIcon = dirname(__FILE__).'/logo.gif';
                $newStateIcon = dirname(__FILE__).'/logo.gif';
                copy($skrillIcon, $newStateIcon);
            }

            Configuration::updateValue($configKey, (int)$orderState->id);
        }
    }

    protected function postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('SKRILL_PAY_ID', Tools::getValue('SKRILL_PAY_ID'));
            Configuration::updateValue('SKRILL_PAY_EMAIL', Tools::getValue('SKRILL_PAY_EMAIL'));
            Configuration::updateValue('SKRILL_RECIPIENT', Tools::getValue('SKRILL_RECIPIENT'));
            Configuration::updateValue('API_PASSWORD', Tools::getValue('API_PASSWORD'));
            Configuration::updateValue('SECRET_PASSWORD', Tools::getValue('SECRET_PASSWORD'));
            Configuration::updateValue('ENABLE_SKRILL', Tools::getValue('ENABLE_SKRILL'));
            Configuration::updateValue('ENABLE_CC', Tools::getValue('ENABLE_CC'));
        }
        $this->_html .= $this->displayConfirmation($this->trans('Settings updated', array(), 'Admin.Global'));
    }

    public function renderForm()
    {
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

         $fields_form_customization = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Customization', array(), 'Modules.SkrillPayment.Admin'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Skrill Merchant ID', array(), 'Modules.SkrillPayment.Admin'),
                        'name' => 'SKRILL_PAY_ID',
                        'required'  => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Skrill Merchant Account', array(), 'Modules.SkrillPayment.Admin'),
                        'name' => 'SKRILL_PAY_EMAIL',
                        'required'  => true,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->trans('Skrill Recipient', array(), 'Modules.SkrillPayment.Admin'),
                        'name' => 'SKRILL_RECIPIENT',
                        'required'  => true,
                    ),
                    array(
                        'type' => 'password',
                        'label' => $this->trans('API Password', array(), 'Modules.SkrillPayment.Admin'),
                        'name' => 'API_PASSWORD',
                        'required'  => true,
                    ),
                    array(
                        'type' => 'password',
                        'label' => $this->trans('Secret Password', array(), 'Modules.SkrillPayment.Admin'),
                        'name' => 'SECRET_PASSWORD',
                        'required'  => true,
                    ),
                ),
            ),
        );

        $fields_enable_payment = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->trans('Enable / Disable Payment', array(), 'Modules.SkrillPayment.Admin'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                      'type'      => 'switch',
                      'label'     => $this->l('Skrill'),
                      'name'      => 'ENABLE_SKRILL',
                      'required'  => true,
                      'class'     => 't',
                      'is_bool'   => true,
                                                                         
                      'values'    => array(
                        array(
                          'id'    => 'active_on',
                          'value' => 1,
                          'label' => $this->l('Enabled')
                        ),
                        array(
                          'id'    => 'active_off',
                          'value' => 0,
                          'label' => $this->l('Disabled')
                        )
                      ),
                    ),
                    array(
                      'type'      => 'switch',
                      'label'     => $this->l('CC'),
                      'name'      => 'ENABLE_CC',
                      'required'  => true,
                      'class'     => 't',
                      'is_bool'   => true,
                                                                         
                      'values'    => array(
                        array(
                          'id'    => 'active_on',
                          'value' => 1,
                          'label' => $this->l('Enabled')
                        ),
                        array(
                          'id'    => 'active_off',
                          'value' => 0,
                          'label' => $this->l('Disabled')
                        )
                      ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->trans('Save'),
                )
            ),
        );

        $helper = new HelperForm();
         
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
         
        $helper->submit_action = 'btnSubmit';

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
            
         return $helper->generateForm(array($fields_form_customization,$fields_enable_payment));
    }

    public function getConfigFieldsValues()
    {
        return array(
            'SKRILL_PAY_ID' => Tools::getValue('SKRILL_PAY_ID', Configuration::get('SKRILL_PAY_ID')),
            'SKRILL_PAY_EMAIL' => Tools::getValue('SKRILL_PAY_EMAIL', Configuration::get('SKRILL_PAY_EMAIL')),
            'SKRILL_RECIPIENT' => Tools::getValue('SKRILL_RECIPIENT', Configuration::get('SKRILL_RECIPIENT')),
            'API_PASSWORD' => Tools::getValue('API_PASSWORD', Configuration::get('API_PASSWORD')),
            'SECRET_PASSWORD' => Tools::getValue('SECRET_PASSWORD', Configuration::get('SECRET_PASSWORD')),
            'ENABLE_SKRILL' => Tools::getValue('ENABLE_SKRILL', Configuration::get('ENABLE_SKRILL')),
            'ENABLE_CC' => Tools::getValue('ENABLE_CC', Configuration::get('ENABLE_CC'))
        );
    }

    public function getContent()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $this->postValidation();
            if (!count($this->postErrors)) {
                $this->postProcess();
            } else {
                foreach ($this->postErrors as $err) {
                    $this->_html .= $this->displayError($err);
                }
            }
        } else {
            $this->_html .= '<br />';
        }

        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    public function hookUpdateOrderStatus($params)
    {
        
        if ($params['newOrderStatus']->id == 7) {
                PrestaShopLogger::addLog('refund process', 1, null, null, 0, true);
                $orderId = $params['id_order'];

                $sidRefund = $this->getRefundSid($orderId);

                $sid = (string)$sidRefund->sid;

                $url = "https://www.skrill.com/app/refund.pl?action=refund&sid=$sid";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, false);
                
                $responseData = curl_exec($ch);
            if (curl_errno($ch)) {
                return curl_error($ch);
            }
                curl_close($ch);

                $responseData = simplexml_load_string($responseData);
                $status = (int)$responseData->status;

            if ($status == 2) {
                PrestaShopLogger::addLog('refund success', 1, null, null, 0, true);
                $this->context->cookie->skrill_status_refund = 'success';
            } elseif ($status == 0) {
                PrestaShopLogger::addLog('refund pending', 1, null, null, 0, true);
                $this->context->cookie->skrill_status_refund = 'pending';
                $this->redirectOrderDetail($orderId);
            } elseif ($status == -2) {
                PrestaShopLogger::addLog('refund fail', 1, null, null, 0, true);
                $this->context->cookie->skrill_status_refund = 'error';
                $this->redirectOrderDetail($orderId);
            }
        }
    }

    public function getRefundSid($order_id)
    {

            $email = Configuration::get('SKRILL_PAY_EMAIL');
            $passwordmd5 = md5(Configuration::get('API_PASSWORD'));

            $query = "SELECT * FROM sid WHERE order_id = '".pSQL($order_id)."'";
            $row = Db::getInstance()->getRow($query);
        if ($row) {
            $transid = $row['transaction_id'];
        }
           $url = "https://www.skrill.com/app/refund.pl";
           $data = "action=prepare&email=$email&password=$passwordmd5&transaction_id=$transid";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
                curl_close($ch);

            return simplexml_load_string($responseData);
    }

    public function hookdisplayInvoice()
    {

        if (isset($this->context->cookie->skrill_status_refund)) {
            $notificationMessage = $this->getRefundedNotificationMessage();
        }

        $tplVars = array();
        $tplVars['warningMessage'] = '';
        $tplVars['successMessage'] = '';
        $tplVars['errorMessage'] = '';

        if (isset($notificationMessage['pending'])) {
            $tplVars['warningMessage'] = $notificationMessage['pending'];
        }
        if (isset($notificationMessage['success'])) {
            $tplVars['successMessage'] = $notificationMessage['success'];
        }
        if (isset($notificationMessage['error'])) {
            $tplVars['errorMessage'] = $notificationMessage['error'];
        }

        $this->context->smarty->assign($tplVars);

        return $this->display(__FILE__, 'views/templates/hook/displayRefundMessage.tpl');
    }

    protected function getRefundedNotificationMessage()
    {
        $notificationMessage = array();
        if ($this->context->cookie->skrill_status_refund == 'pending') {
            $notificationMessage['pending'] = "refund";
        } elseif ($this->context->cookie->skrill_status_refund == 'success') {
            $notificationMessage['success'] = "refund";
        } else {
            $notificationMessage['error'] = "refund";
        }
        unset($this->context->cookie->skrill_status_refund);

        return $notificationMessage;
    }

    protected function redirectOrderDetail($orderId)
    {
        $getAdminLink = $this->context->link->getAdminLink('AdminOrders');
        $getViewOrder = $getAdminLink.'&vieworder&id_order='.$orderId;
        Tools::redirectAdmin($getViewOrder);
    }

    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }

        $skrill_enable = Configuration::get('ENABLE_SKRILL');
        $cc_enable = Configuration::get('ENABLE_CC');

        $logo = Media::getMediaPath(_PS_MODULE_DIR_.'skrillpayment/views/img/skrill.png');
        $logo2 = Media::getMediaPath(_PS_MODULE_DIR_.'skrillpayment/views/img/cc.png');

        if ($skrill_enable == 1 && $cc_enable == 1) {
            $paymentOption = new PaymentOption();
            $paymentOption
            ->setCallToActionText($this->trans('', array(), 'Modules.SkrillPayment.Shop'))
            ->setAction($this->context->link->getModuleLink($this->name, 'process', array('paymentmthd'=>'WLT'), true))
            ->setLogo($logo);

            $paymentOption2 = new PaymentOption();
            $paymentOption2
            ->setCallToActionText($this->trans('', array(), 'Modules.SkrillPayment.Shop'))
            ->setAction($this->context->link->getModuleLink($this->name, 'process', array('paymentmthd'=>'ACC'), true))
            ->setLogo($logo2);

            $payment_options = array(
            $paymentOption, $paymentOption2
            );
            return $payment_options;
        } elseif ($skrill_enable == 0 && $cc_enable == 1) {
            $paymentOption2 = new PaymentOption();
            $paymentOption2
            ->setCallToActionText($this->trans('', array(), 'Modules.SkrillPayment.Shop'))
            ->setAction($this->context->link->getModuleLink($this->name, 'process', array('paymentmthd'=>'ACC'), true))
            ->setLogo($logo2);

            $payment_options = array(
            $paymentOption2
            );
            return $payment_options;
        } elseif ($skrill_enable == 1 && $cc_enable == 0) {
            $paymentOption = new PaymentOption();
            $paymentOption
                ->setCallToActionText($this->trans('', array(), 'Modules.SkrillPayment.Shop'))
            ->setAction($this->context->link->getModuleLink($this->name, 'process', array('paymentmthd'=>'WLT'), true))
                ->setLogo($logo);

            $payment_options = array(
            $paymentOption
            );
            return $payment_options;
        } else {
            return;
        }
    }
}
