<?php
/**
*  @author    Sutrisno Denny
*  @copyright DS 2017
*  @license   DS ESPHERE @2017
*  @version   1.0.1
*
* Languages: EN
* PS version: 1.7.0.6
**/

class SkrillPaymentValidationModuleFrontController extends ModuleFrontController
{

    public function postProcess()
    {
        PrestaShopLogger::addLog('validating process', 1, null, null, 0, true);
        $transId = Tools::getValue('transaction_id');
        $cartId = Tools::getValue('cart_id');
        $secure_key = Tools::getValue('secure_key');

        $query = "SELECT * FROM sid WHERE transaction_id = '".pSQL($transId)."'";
            $row = Db::getInstance()->getRow($query);
        if ($row) {
            $orderid = (int)$row['order_id'];
            $status  = (int)$row['status'];
        }

        if ($status == 2 || $status == 0) {
            PrestaShopLogger::addLog('redirecting to payment success page', 1, null, null, 0, true);
            $url = 'index.php?controller=order-confirmation&id_cart='.$cartId.
            '&id_module='.$this->module->id.'&id_order='.$orderid.'&key='.$secure_key.'';
            Tools::redirect($url);
        } elseif ($status == -2) {
            PrestaShopLogger::addLog('redirecting to order page', 1, null, null, 0, true);
            $this->errors[] = $this->getLocaleErrorMapping();
           
            $this->redirectWithNotifications(
                $this->context->link->getPageLink(
                    'order',
                    true,
                    null,
                    array('step' => '3')
                )
            );
        }
    }

    public function getLocaleErrorMapping()
    {
        $returnMessage = 'Process payment fail, please try again';
        return $returnMessage;
    }
}
