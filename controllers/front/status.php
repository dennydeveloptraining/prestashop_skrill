<?php
/**
*  @author    Sutrisno Denny
*  @copyright DS 2017
*  @license   DS ESPHERE @2017
*  @version   1.0.1
*
* Languages: EN
* PS version: 1.7.0.6
**/

class SkrillPaymentStatusModuleFrontController extends ModuleFrontController
{
    public function postProcess()
    {
        PrestaShopLogger::addLog('process myskrill payment status', 1, null, null, 0, true);
        $paymentResponse = $this->getPaymentResponse();
        $messageLog = 'payment response from status_url : ' . print_r($paymentResponse, true);
        PrestaShopLogger::addLog($messageLog, 1, null, null, 0, true);
        $transactionLog = array();
        $transactionLog = $this->setTransactionLog($paymentResponse);

        $transId = $transactionLog['transaction_id'];
        $status = (int)$transactionLog['status'];
        $cartId = $transactionLog['cart_id'];
        $secure_key = $transactionLog['secure_key'];
        $amount = $transactionLog['amount'];
        $currency = $transactionLog['currency'];

        if ($status == 2) {
            PrestaShopLogger::addLog('update order status payment accepted', 1, null, null, 0, true);
            $this->module->validateOrder(
                $cartId,
                Configuration::get('PS_OS_PAYMENT'),
                $amount,
                $this->module->displayName,
                null,
                null,
                (int)$currency->id,
                false,
                $secure_key
            );
        } elseif ($status == 0) {
            PrestaShopLogger::addLog('update order status payment pending', 1, null, null, 0, true);
            $this->module->validateOrder(
                $cartId,
                Configuration::get('SKRILL_PAYMENT_STATUS_PENDING'),
                $amount,
                $this->module->displayName,
                null,
                null,
                (int)$currency->id,
                false,
                $secure_key
            );
        } elseif ($status == -2) {
            PrestaShopLogger::addLog('update order status payment error', 1, null, null, 0, true);
            $this->module->validateOrder(
                $cartId,
                Configuration::get('PS_OS_ERROR'),
                $amount,
                $this->module->displayName,
                null,
                null,
                (int)$currency->id,
                false,
                $secure_key
            );
        }
        $orderId = $this->module->currentOrder;
            $query = "INSERT INTO sid(order_id,transaction_id,status) VALUES ('$orderId','$transId','$status')";
            Db::getInstance()->execute($query);
    }

    protected function getPaymentResponse()
    {
        PrestaShopLogger::addLog('getting payment response', 1, null, null, 0, true);
        $paymentResponse = array();
        foreach ($_REQUEST as $parameter => $value) {
            $parameter = Tools::strtolower($parameter);
            $paymentResponse[$parameter] = $value;
        }

        return $paymentResponse;
    }

    protected function setTransactionLog($paymentResponse)
    {
        PrestaShopLogger::addLog('setting transaction log from response', 1, null, null, 0, true);
        $transactionLog = array();

        $transactionLog['cart_id'] = $paymentResponse['cart_id'];
        $transactionLog['secure_key'] = $paymentResponse['secure_key'];
        $transactionLog['transaction_id'] = $paymentResponse['transaction_id'];
        $transactionLog['mb_transaction_id'] = $paymentResponse['mb_transaction_id'];
        $transactionLog['status'] = $paymentResponse['status'];
        $transactionLog['currency'] = $paymentResponse['currency'];
        $transactionLog['amount'] = $paymentResponse['amount'];
        $transactionLog['payment_response'] = serialize($paymentResponse);

        return $transactionLog;
    }
}
