<?php
/**
*  @author    Sutrisno Denny
*  @copyright DS 2017
*  @license   DS ESPHERE @2017
*  @version   1.0.1
*
* Languages: EN
* PS version: 1.7.0.6
**/

class SkrillPaymentProcessModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

         $postParameters = $this->getPostParameters();
         $sid = $this->getSid($postParameters);
         $redirectUrl = $this->getSkrillRedirectUrl($sid);

        $this->context->smarty->assign(
            array(
            'redirectUrl' => $redirectUrl,
            )
        );

        $this->setTemplate('module:skrillpayment/views/templates/front/skrill_form.tpl');
    }

    public function getSkrillSettings()
    {
            $skrillSettings = array();

            $skrillSettings['id'] = Configuration::get('SKRILL_PAY_ID');
            $skrillSettings['email'] = Configuration::get('SKRILL_PAY_EMAIL');
            $skrillSettings['recipient'] = Configuration::get('SKRILL_RECIPIENT');
            $skrillSettings['api_password'] = Configuration::get('SKRILL_API_PASSWORD');
            $skrillSettings['secret_password'] = Configuration::get('SKRILL_SECRET_PASSWORD');

        return $skrillSettings;
    }

    public function getPostParameters()
    {
            $cart = $this->context->cart;
            $contextLink = $this->context->link;
            $payment_method = Tools::getValue('paymentmthd');

              $customer = new Customer($cart->id_customer);
            $address = new Address((int)$cart->id_address_delivery);
            $country = new Country($address->id_country);
            $currency = new Currency((int)$cart->id_currency);

            $skrillSettings = $this->getSkrillSettings();
            $dateTime = $this->getDateTime();
            $randomNumber = $this->randomNumber();

            $skrillPost = array();

            $skrillPost['pay_to_email'] = $skrillSettings['email'];
            $skrillPost['language'] = $this->getLang();
            $skrillPost['transaction_id'] = date('ymd') . $cart->id . $dateTime . $randomNumber;
            $skrillPost['return_url'] = $contextLink->getModuleLink(
                'skrillpayment',
                'validation',
                array('cart_id' => $cart->id, 'secure_key' => $customer->secure_key),
                true
            );
            $skrillPost['status_url'] = $contextLink->getModuleLink(
                'skrillpayment',
                'status',
                array('cart_id' => $cart->id, 'secure_key' => $customer->secure_key),
                true
            );
            $skrillPost['status_url2'] = 'mailto:ferdinandus.denny@yahoo.com';
            $skrillPost['cancel_url'] = $contextLink->getPageLink('order', true, null, array('step' => '3'));
            $skrillPost['pay_from_email'] = $this->context->customer->email;
            $skrillPost['payment_methods'] = $payment_method;
            $skrillPost['amount'] = $cart->getOrderTotal(true, Cart::BOTH);
            $skrillPost['currency'] = $currency->iso_code;
            $skrillPost['firstname'] = $this->context->customer->firstname;
            $skrillPost['lastname'] = $this->context->customer->lastname;
            $skrillPost['pay_from_email'] = $this->context->customer->email;
            $skrillPost['address'] = $address->address1;
            $skrillPost['postal_code'] = $address->postcode;
            $skrillPost['city'] = $address->city;
            $skrillPost['country'] = $this->getCountryIso3($country->iso_code);
            $skrillPost['recipient_description'] = $skrillSettings['recipient'];
            $skrillPost['prepare_only'] = 1;
            $skrillPost['detail1_description'] = 'Payment for : ' . $this->context->customer->email;
            $skrillPost['merchant_fields'] = 'Platform,Developer';
            $skrillPost['Developer'] = 'Esphere';

            return $skrillPost;
    }

    public static function randomNumber($length)
    {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
              $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public static function getDateTime()
    {
            $t = microtime(true);
            $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
            $d = new DateTime(date('Y-m-d H:i:s.'.$micro, $t));

            return $d->format("ymdhiu");
    }

    public function getLang()
    {
        $cart = $this->context->cart;
        $language = new Language((int)$cart->id_lang);
        $languageCode = $language->iso_code;

        switch ($languageCode) {
            case 'de':
            case 'pl':
            case 'it':
            case 'fr':
            case 'es':
                return $languageCode;
        }

        return 'en';
    }

    public static function getSid($fields)
    {
        $fields_string = http_build_query($fields);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://pay.skrill.com");
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded;charset=UTF-8'));
        curl_setopt($curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($curl, CURLOPT_POST, count($fields));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);

            $result = curl_exec($curl);
        if (curl_errno($curl)) {
            throw new Exception("Curl error: ". curl_error($curl));
        }
        curl_close($curl);

        return $result;
    }

    public static function getSkrillRedirectUrl($sid)
    {
        $skrillRedirectUrl = 'https://pay.skrill.com?sid='.$sid;
        return $skrillRedirectUrl;
    }

    public static function getCountryIso3($iso2)
    {
            $iso3 = array(
                  'AF' => 'AFG',
                  'AL' => 'ALB',
                  'DZ' => 'DZA',
                  'AS' => 'ASM',
                  'AD' => 'AND',
                  'AO' => 'AGO',
                  'AI' => 'AIA',
                  'AQ' => 'ATA',
                  'AG' => 'ATG',
                  'AR' => 'ARG',
                  'AM' => 'ARM',
                  'AW' => 'ABW',
                  'AU' => 'AUS',
                  'AT' => 'AUT',
                  'AZ' => 'AZE',
                  'BS' => 'BHS',
                  'BH' => 'BHR',
                  'BD' => 'BGD',
                  'BB' => 'BRB',
                  'BY' => 'BLR',
                  'BE' => 'BEL',
                  'BZ' => 'BLZ',
                  'BJ' => 'BEN',
                  'BM' => 'BMU',
                  'BT' => 'BTN',
                  'BO' => 'BOL',
                  'BA' => 'BIH',
                  'BW' => 'BWA',
                  'BV' => 'BVT',
                  'BR' => 'BRA',
                  'IO' => 'IOT',
                  'VG' => 'VGB',
                  'BN' => 'BRN',
                  'BG' => 'BGR',
                  'BF' => 'BFA',
                  'BI' => 'BDI',
                  'KH' => 'KHM',
                  'CM' => 'CMR',
                  'CA' => 'CAN',
                  'CV' => 'CPV',
                  'KY' => 'CYM',
                  'CF' => 'CAF',
                  'TD' => 'TCD',
                  'CL' => 'CHL',
                  'CN' => 'CHN',
                  'CX' => 'CXR',
                  'CC' => 'CCK',
                  'CO' => 'COL',
                  'KM' => 'COM',
                  'CG' => 'COG',
                  'CD' => 'COD',
                  'CK' => 'COK',
                  'CR' => 'CRI',
                  'HR' => 'HRV',
                  'CU' => 'CUB',
                  'CY' => 'CYP',
                  'CZ' => 'CZE',
                  'CI' => 'CIV',
                  'DK' => 'DNK',
                  'DJ' => 'DJI',
                  'DM' => 'DMA',
                  'DO' => 'DOM',
                  'EC' => 'ECU',
                  'EG' => 'EGY',
                  'SV' => 'SLV',
                  'GQ' => 'GNQ',
                  'ER' => 'ERI',
                  'EE' => 'EST',
                  'ET' => 'ETH',
                  'FK' => 'FLK',
                  'FO' => 'FRO',
                  'FJ' => 'FJI',
                  'FI' => 'FIN',
                  'FR' => 'FRA',
                  'GF' => 'GUF',
                  'PF' => 'PYF',
                  'TF' => 'ATF',
                  'GA' => 'GAB',
                  'GM' => 'GMB',
                  'GE' => 'GEO',
                  'DE' => 'DEU',
                  'GH' => 'GHA',
                  'GI' => 'GIB',
                  'GR' => 'GRC',
                  'GL' => 'GRL',
                  'GD' => 'GRD',
                  'GP' => 'GLD',
                  'GU' => 'GUM',
                  'GT' => 'GTM',
                  'GG' => 'GGY',
                  'GN' => 'HTI',
                  'GW' => 'HMD',
                  'GY' => 'VAT',
                  'HT' => 'GIN',
                  'HM' => 'GNB',
                  'HN' => 'HND',
                  'HK' => 'HKG',
                  'HU' => 'HUN',
                  'IS' => 'ISL',
                  'IN' => 'IND',
                  'ID' => 'IDN',
                  'IR' => 'IRN',
                  'IQ' => 'IRQ',
                  'IE' => 'IRL',
                  'IM' => 'IMN',
                  'IL' => 'ISR',
                  'IT' => 'ITA',
                  'JM' => 'JAM',
                  'JP' => 'JPN',
                  'JE' => 'JEY',
                  'JO' => 'JOR',
                  'KZ' => 'KAZ',
                  'KE' => 'KEN',
                  'KI' => 'KIR',
                  'KW' => 'KWT',
                  'KG' => 'KGZ',
                  'LA' => 'LAO',
                  'LV' => 'LVA',
                  'LB' => 'LBN',
                  'LS' => 'LSO',
                  'LR' => 'LBR',
                  'LY' => 'LBY',
                  'LI' => 'LIE',
                  'LT' => 'LTU',
                  'LU' => 'LUX',
                  'MO' => 'MAC',
                  'MK' => 'MKD',
                  'MG' => 'MDG',
                  'MW' => 'MWI',
                  'MY' => 'MYS',
                  'MV' => 'MDV',
                  'ML' => 'MLI',
                  'MT' => 'MLT',
                  'MH' => 'MHL',
                  'MQ' => 'MTQ',
                  'MR' => 'MRT',
                  'MU' => 'MUS',
                  'YT' => 'MYT',
                  'MX' => 'MEX',
                  'FM' => 'FSM',
                  'MD' => 'MDA',
                  'MC' => 'MCO',
                  'MN' => 'MNG',
                  'ME' => 'MNE',
                  'MS' => 'MSR',
                  'MA' => 'MAR',
                  'MZ' => 'MOZ',
                  'MM' => 'MMR',
                  'NA' => 'NAM',
                  'NR' => 'NRU',
                  'NP' => 'NPL',
                  'NL' => 'NLD',
                  'AN' => 'ANT',
                  'NC' => 'NCL',
                  'NZ' => 'NZL',
                  'NI' => 'NIC',
                  'NE' => 'NER',
                  'NG' => 'NGA',
                  'NU' => 'NIU',
                  'NF' => 'NFK',
                  'KP' => 'PRK',
                  'MP' => 'MNP',
                  'NO' => 'NOR',
                  'OM' => 'OMN',
                  'PK' => 'PAK',
                  'PW' => 'PLW',
                  'PS' => 'PSE',
                  'PA' => 'PAN',
                  'PG' => 'PNG',
                  'PY' => 'PRY',
                  'PE' => 'PER',
                  'PH' => 'PHL',
                  'PN' => 'PCN',
                  'PL' => 'POL',
                  'PT' => 'PRT',
                  'PR' => 'PRI',
                  'QA' => 'QAT',
                  'RO' => 'ROU',
                  'RU' => 'RUS',
                  'RW' => 'RWA',
                  'RE' => 'REU',
                  'BL' => 'BLM',
                  'SH' => 'SHN',
                  'KN' => 'KNA',
                  'LC' => 'LCA',
                  'MF' => 'MAF',
                  'PM' => 'SPM',
                  'WS' => 'WSM',
                  'SM' => 'SMR',
                  'SA' => 'SAU',
                  'SN' => 'SEN',
                  'RS' => 'SRB',
                  'SC' => 'SYC',
                  'SL' => 'SLE',
                  'SG' => 'SGP',
                  'SK' => 'SVK',
                  'SI' => 'SVN',
                  'SB' => 'SLB',
                  'SO' => 'SOM',
                  'ZA' => 'ZAF',
                  'GS' => 'SGS',
                  'KR' => 'KOR',
                  'ES' => 'ESP',
                  'LK' => 'LKA',
                  'VC' => 'VCT',
                  'SD' => 'SDN',
                  'SR' => 'SUR',
                  'SJ' => 'SJM',
                  'SZ' => 'SWZ',
                  'SE' => 'SWE',
                  'CH' => 'CHE',
                  'SY' => 'SYR',
                  'ST' => 'STP',
                  'TW' => 'TWN',
                  'TJ' => 'TJK',
                  'TZ' => 'TZA',
                  'TH' => 'THA',
                  'TL' => 'TLS',
                  'TG' => 'TGO',
                  'TK' => 'TKL',
                  'TO' => 'TON',
                  'TT' => 'TTO',
                  'TN' => 'TUN',
                  'TR' => 'TUR',
                  'TM' => 'TKM',
                  'TC' => 'TCA',
                  'TV' => 'TUV',
                  'UM' => 'UMI',
                  'VI' => 'VIR',
                  'UG' => 'UGA',
                  'UA' => 'UKR',
                  'AE' => 'ARE',
                  'GB' => 'GBR',
                  'US' => 'USA',
                  'UY' => 'URY',
                  'UZ' => 'UZB',
                  'VU' => 'VUT',
                  'VA' => 'GUY',
                  'VE' => 'VEN',
                  'VN' => 'VNM',
                  'WF' => 'WLF',
                  'EH' => 'ESH',
                  'YE' => 'YEM',
                  'ZM' => 'ZMB',
                  'ZW' => 'ZWE',
                  'AX' => 'ALA'
        );
        if ($iso2) {
            return array_key_exists($iso2, $iso3) ? $iso3[$iso2] : '';
        } else {
            return '';
        }
    }
}
